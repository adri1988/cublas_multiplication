#include <cuda.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <math.h> 
#include <cuda_runtime.h> 
#include "cublas_v2.h"

void init_matrix(float *M, int hM, int wM, float k)
{
	int i,j;

	for (i=0; i<hM; i++)
		for (j=0; j<wM; j++)
			if (i==j)
				M[i*wM+j] = k*k*1.0f;
			else
				M[i*wM+j] = -k*1.0f;
}

void print_matrix(float *M, int hM, int wM)
{
	int i,j;

	for (i=0; i<hM; i++){
		for (j=0; j<wM; j++)
			printf("%4.1f ", M[i*wM+j]);
		printf("\n");
	}
}

void Mul(float* A, float* B, int hA, int wA, int wB,
	float* C)// multiply A*B and store the result in C
{
	int size;
	// Load A and B to the device
	float* Ad;
	size = hA * wA * sizeof(float);
	cudaMalloc((void**)&Ad, size);
	cudaMemcpy(Ad,A,size,cudaMemcpyHostToDevice);
	
	float* Bd;
	size = wA * wB * sizeof(float);
	cudaMalloc((void**)&Bd, size);
	cudaMemcpy(Bd,B,size,cudaMemcpyHostToDevice);

	// Allocate C on the device
	float* Cd;
	size = hA * wB * sizeof(float);
	cudaMalloc((void**)&Cd, size);	
	
	//declare alfa and beta
	const float alfa = 1;
    const float beta = 0;
    const float *alphac = &alfa;
    const float *betac = &beta;
	
	//declare lda ldb and ldc (not used tho)
	int lda=wA,ldb=hA,ldc=wA;
	
	// handle declaration and creation	
	cublasHandle_t handle;
	cublasCreate(&handle);
	
	//cublas method call
	cublasSgemm(handle, 	CUBLAS_OP_N, 	CUBLAS_OP_N, 	wA, 	wB, 	hA, 	alphac, 	Ad, 	lda, 	Bd, 	ldb, 	betac, 	Cd, 	ldc);	
	
	//handle destruction
	cublasDestroy(handle);
	// Read C from the device
	cudaMemcpy(C, Cd, size, cudaMemcpyDeviceToHost);
	// Free device memory
	cudaFree(Ad);
	cudaFree(Bd);
	cudaFree(Cd);
}

int main(int argc, char *argv[])
{
	// Matrix variables
	float *A, *B, *C;
	int hA, wA, hB, wB;

	setbuf(stdout, NULL);

	if (argc!=4){
		printf("./exec hA hB/WA wB\n");
		exit(-1);
	}

	hA = atoi(argv[1]);
	hB = wA = atoi(argv[2]);
	wB = atoi(argv[3]);

	// Init A and B, malloc C
	int size_A = wA * hA;
	A = (float*)malloc(size_A*sizeof(float));
	init_matrix(A, hA, wA,2.0);

	int size_B = wB * hB;
	B = (float*)malloc(size_B*sizeof(float));
	init_matrix(B, hB, wB,1.0);

	int size_C = wB * hA;
	C = (float*)malloc(size_C*sizeof(float));

	//multiplication and print on console
	Mul(A, B, hA, wA, wB, C);
	printf("\n\nMATRIX A\n");print_matrix(A, hA, wA);
	printf("\n\nMATRIX B\n");print_matrix(B, hB, wB);
	printf("\n\nMATRIX C\n");print_matrix(C, hA, wB);

	//final touches
	free(A);
	free(B);
	free(C);
	

	return (1);
}


							
